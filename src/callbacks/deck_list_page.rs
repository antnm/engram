use crate::{model::DeckStore, window::Window};
use gtk::prelude::*;

pub struct DeckListPage {}

#[gtk::template_callbacks]
impl DeckListPage {
    #[template_callback(function)]
    fn deck_name_valid(name: &str) -> bool {
        !name.is_empty() && !DeckStore::name_exists(name)
    }

    #[template_callback]
    fn add_deck(window: &Window) {
        if window.add_deck_button.is_sensitive() {
            window.add_deck_popover.popdown();
            window
                .deck_store
                .add(window.add_deck_entry.text().to_string());
            window.add_deck_entry.set_text("");
        }
    }
}
