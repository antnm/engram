use crate::{model::Deck, window::Window};
use adw::prelude::*;
use gtk::RevealerTransitionType;

pub struct AddCardPage {}

#[gtk::template_callbacks]
impl AddCardPage {
    #[template_callback]
    fn add_card_page_go_back(window: &Window) {
        window.stack.set_visible_child(&*window.leaflet);
        window.add_card_front_side_toggle_button.set_active(true);
        window.add_card_front_text.clear();
        window.add_card_back_text.clear();
    }

    #[template_callback(function)]
    fn add_card_revealer_transition(shown: bool) -> RevealerTransitionType {
        if shown {
            RevealerTransitionType::SlideDown
        } else {
            RevealerTransitionType::SlideLeft
        }
    }

    #[template_callback(function)]
    fn card_sides_valid(front: &str, back: &str) -> bool {
        !front.is_empty() && !back.is_empty()
    }

    #[template_callback]
    fn add_card(window: &Window) {
        window
            .deck_store_selection
            .selected_item()
            .unwrap()
            .downcast::<Deck>()
            .unwrap()
            .add_card(
                &window.card_store,
                &window.add_card_front_text.text(),
                &window.add_card_back_text.text(),
            );
        window.add_card_front_side_toggle_button.set_active(true);
        window.add_card_front_text.clear();
        window.add_card_back_text.clear();
    }
}
