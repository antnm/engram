use crate::{
    model::{Deck, DeckStore},
    window::Window,
};
use adw::{prelude::*, Toast};

pub struct DeckPage {}

#[gtk::template_callbacks]
impl DeckPage {
    #[template_callback]
    fn deck_page_go_back(window: &Window) {
        window
            .deck_store_selection
            .set_selected(gtk::INVALID_LIST_POSITION);
    }

    #[template_callback]
    fn cancel_deck_name_edit(window: &Window) {
        window.deck_name_edit_button.set_active(false);
    }

    #[template_callback]
    fn finish_deck_name_edit(window: &Window) {
        if window.deck_name_edit_button.is_sensitive() {
            window
                .deck_store_selection
                .selected_item()
                .unwrap()
                .downcast::<Deck>()
                .unwrap()
                .set_name(window.deck_name_entry.text());
            window.deck_name_edit_button.set_active(false);
        }
    }

    #[template_callback]
    fn toggle_deck_name_edit(window: &Window) {
        if window.deck_name_edit_button.is_active() {
            window
                .deck_name_entry
                .set_text(&window.deck_name_label.label());
            window.deck_name_entry.grab_focus();
        } else {
            window
                .deck_store_selection
                .selected_item()
                .unwrap()
                .downcast::<Deck>()
                .unwrap()
                .set_name(window.deck_name_entry.text());
        }
    }

    #[template_callback]
    fn show_add_card_page(window: &Window) {
        window.stack.set_visible_child(&*window.add_card_page);
    }

    #[template_callback]
    fn delete_deck(window: &Window) {
        let deck = window
            .deck_store_selection
            .selected_item()
            .unwrap()
            .downcast::<Deck>()
            .unwrap();
        window.deck_store.delete(deck.id());
        let toast = Toast::builder()
            .title(gettextrs::gettext!("\"{}\" deleted", deck.name()))
            .button_label(gettextrs::gettext("Undo"))
            .build();
        toast.connect_dismissed(
            glib::clone!(@weak window => move |_| window.deck_store.confirm_delete()),
        );
        toast.connect_button_clicked(
            glib::clone!(@weak window => move |_| window.deck_store.undo_delete()),
        );
        window.toast_overlay.add_toast(toast);
    }

    #[template_callback]
    fn format_recall_probability_target_spin_button(window: &Window) -> bool {
        window
            .recall_probability_target_spin_button
            .set_text(&format!(
                "{}%",
                window.recall_probability_target_adjustment.value()
            ));
        true
    }

    #[template_callback]
    fn recall_probability_target_spin_button_changed(window: &Window) {
        window
            .deck_store_selection
            .selected_item()
            .unwrap()
            .downcast::<Deck>()
            .unwrap()
            .set_recall_probability_target_ln(
                (window.recall_probability_target_adjustment.value() / 100.0).ln(),
            );
    }

    #[template_callback(function)]
    fn recall_percentage(value: f64) -> f32 {
        (value.exp() * 100.0) as f32
    }

    #[template_callback(function)]
    fn deck_rename_valid(editing: bool, name: &str, old_name: &str) -> bool {
        !editing || name == old_name || !DeckStore::name_exists(name)
    }

    #[template_callback(function)]
    fn cards_to_review_count_subtitle(review_count: u32) -> String {
        gettextrs::ngettext!(
            "{} card to review",
            "{} cards to review",
            review_count,
            review_count
        )
    }

    #[template_callback(function)]
    fn cards_count_subtitle(count: u32) -> String {
        gettextrs::ngettext!("{} card in total", "{} cards in total", count, count)
    }
}
