use gtk::RevealerTransitionType;

pub struct CardEditor {}

#[gtk::template_callbacks]
impl CardEditor {
    #[template_callback(function)]
    fn add_card_revealer_transition(shown: bool) -> RevealerTransitionType {
        if shown {
            RevealerTransitionType::SlideDown
        } else {
            RevealerTransitionType::SlideLeft
        }
    }

    #[template_callback(function)]
    fn card_sides_valid(front: &str, back: &str) -> bool {
        !front.is_empty() && !back.is_empty()
    }
}
