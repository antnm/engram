use gtk::INVALID_LIST_POSITION;

pub struct Utils {}

#[gtk::template_callbacks(functions)]
impl Utils {
    #[template_callback]
    fn if_else(cond: bool, if_value: glib::Value, else_value: glib::Value) -> glib::Value {
        if cond {
            if_value
        } else {
            else_value
        }
    }

    #[template_callback]
    fn valid_list_position(position: u32) -> bool {
        position != INVALID_LIST_POSITION
    }

    #[template_callback]
    fn non_zero(n: u32) -> bool {
        n != 0
    }
}
