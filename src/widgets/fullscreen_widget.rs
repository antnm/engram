mod imp {
    use crate::window::WINDOW;
    use adw::{prelude::*, subclass::prelude::*, Bin};
    use glib::{Object, Properties, SourceId};
    use gtk::{
        template_callbacks, Align, Button, CallbackAction, CompositeTemplate, Overlay, Revealer,
        ShortcutController, ShortcutTrigger, TemplateChild, Widget,
    };
    use once_cell::sync::OnceCell;
    use std::{
        cell::{Cell, RefCell},
        time::Duration,
    };

    #[derive(CompositeTemplate, Default, Properties)]
    #[properties(wrapper_type = super::FullscreenWidget)]
    #[template(resource = "/com/gitlab/antnm/Engram/ui/fullscreen_widget.ui")]
    pub struct FullscreenWidget {
        #[property(type = Option<Widget>, get = |s: &Self| s.content.get().cloned(), set, construct_only)]
        content: OnceCell<Widget>,
        #[property(type = Option<Widget>, get = |s: &Self| s.controls.get().cloned(), set, construct_only)]
        controls: OnceCell<Widget>,
        custom_controls: OnceCell<bool>,
        #[template_child]
        controls_revealer: TemplateChild<Revealer>,
        #[template_child]
        overlay: TemplateChild<Overlay>,
        #[template_child]
        overlay_shortcut_controller: TemplateChild<ShortcutController>,
        hide_controls_source_id: RefCell<Option<SourceId>>,
        last_window_stack_child: RefCell<Option<Widget>>,
        last_pointer_position: Cell<(f64, f64)>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FullscreenWidget {
        const NAME: &'static str = "FullscreenWidget";
        type Type = super::FullscreenWidget;
        type ParentType = Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[template_callbacks]
    impl FullscreenWidget {
        #[template_callback]
        fn show_controls(&self) {
            if !*self.custom_controls.get().unwrap() && !self.obj().is_fullscreen() {
                return;
            }
            self.controls_revealer.set_reveal_child(true);
            let ref_counted = self.ref_counted();
            let old_source_id =
                self.hide_controls_source_id
                    .replace(Some(glib::timeout_add_local_once(
                        Duration::from_secs(5),
                        glib::clone!(@weak ref_counted => move || {
                            ref_counted.controls_revealer.set_reveal_child(false);
                            ref_counted.hide_controls_source_id.replace(None);
                        }),
                    )));
            if let Some(old_source_id) = old_source_id {
                old_source_id.remove();
            }
        }

        #[template_callback]
        fn motion(&self, x: f64, y: f64) {
            if (x, y) != self.last_pointer_position.replace((x, y)) {
                self.show_controls();
            }
        }

        #[template_callback]
        fn clicked(&self, n_press: i32) {
            if n_press % 2 == 0 {
                let obj = self.obj();
                obj.toggle_fullscreen();
            } else if !self.custom_controls.get().unwrap() {
                let obj = self.obj();
                if !obj.is_fullscreen() {
                    obj.toggle_fullscreen();
                }
            }
        }
    }

    impl super::FullscreenWidget {
        pub fn new(content: impl IsA<Widget>, controls: Option<impl IsA<Widget>>) -> Self {
            if let Some(controls) = controls {
                let object: Self = Object::builder()
                    .property("content", &content)
                    .property("controls", &controls)
                    .build();
                object.imp().custom_controls.set(true).unwrap();
                object
            } else {
                let fullscreen_button = Button::builder()
                    .child(
                        &gtk::Image::builder()
                            .icon_name("window-close-symbolic")
                            .icon_size(gtk::IconSize::Large)
                            .build(),
                    )
                    .css_classes(vec!["osd".to_string(), "circular".to_string()])
                    .margin_top(12)
                    .margin_end(12)
                    .halign(Align::End)
                    .valign(Align::Start)
                    .build();
                let object: Self = Object::builder()
                    .property("content", &content)
                    .property("controls", &fullscreen_button)
                    .build();
                fullscreen_button.connect_clicked(glib::clone!(@weak object => move |_| {
                    object.toggle_fullscreen();
                }));
                object.imp().custom_controls.set(false).unwrap();
                object
            }
        }

        pub fn toggle_fullscreen(&self) {
            WINDOW.with(|window| {
                let window = window.get().unwrap();
                let stack = window.stack();
                if let Some(overlay) = self.child() {
                    self.set_child(None::<&Widget>);
                    self.imp()
                        .last_window_stack_child
                        .replace(stack.visible_child());
                    stack.add_child(&overlay);
                    stack.set_visible_child(&overlay);
                    window.fullscreen();
                } else {
                    window.unfullscreen();
                    let overlay = stack.visible_child().unwrap();
                    let imp = self.imp();
                    stack.set_visible_child(&imp.last_window_stack_child.replace(None).unwrap());
                    stack.remove(&overlay);
                    self.set_child(Some(&overlay));
                    if !imp.custom_controls.get().unwrap() {
                        imp.controls_revealer.set_reveal_child(false);
                    }
                }
            });
        }

        fn is_fullscreen(&self) -> bool {
            self.child().is_none()
        }
    }

    impl ObjectImpl for FullscreenWidget {
        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec);
        }

        fn constructed(&self) {
            let widget = self.obj();

            let action = CallbackAction::new(
                glib::clone!(@weak widget => @default-return true, move |_, _| {
                    if widget.is_fullscreen() {
                        widget.toggle_fullscreen();
                    }
                    true
                }),
            );
            let shortcut = gtk::Shortcut::new(
                Some(ShortcutTrigger::parse_string("Escape").unwrap()),
                Some(action),
            );
            self.overlay_shortcut_controller.add_shortcut(shortcut);
        }
    }

    impl WidgetImpl for FullscreenWidget {
        fn unrealize(&self) {
            self.parent_unrealize();
            let obj = self.obj();
            if obj.is_fullscreen() {
                obj.toggle_fullscreen();
            }
        }
    }

    impl BinImpl for FullscreenWidget {}
}

glib::wrapper! {
    pub struct FullscreenWidget(ObjectSubclass<imp::FullscreenWidget>)
        @extends adw::Bin, gtk::Widget;
}
