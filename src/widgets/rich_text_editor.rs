mod imp {
    use std::{cell::RefCell, marker::PhantomData, rc::Rc};

    use crate::{
        application::Application,
        callbacks,
        widgets::{AudioControls, FullscreenWidget, VideoControls},
        window::WINDOW,
    };
    use adw::{prelude::*, subclass::prelude::*, MessageDialog};
    use gdk::{
        pango::{Style, Underline},
        Texture,
    };
    use gio::File;
    use glib::{Downgrade, Object, Properties};
    use gtk::template_callbacks;
    use gtk::{
        ActionBar, Box, CompositeTemplate, FileChooserAction, FileChooserDialog, FileFilter,
        MediaFile, Picture, ResponseType, TemplateChild, TextBuffer, TextIter, TextMark, TextTag,
        TextTagTable, TextView, ToggleButton, Widget,
    };
    use once_cell::unsync::Lazy;

    const TAGS_COUNT: usize = 5;

    pub struct SharedTags {
        table: TextTagTable,
        tags: [TextTag; TAGS_COUNT],
    }

    thread_local! {
        static SHARED_TAGS: Lazy<SharedTags> = Lazy::new(|| {
            let table = TextTagTable::new();
            let tags = [
                TextTag::builder()
                    .weight(600)
                    .accumulative_margin(true)
                    .build(),
                TextTag::builder().style(Style::Italic).build(),
                TextTag::builder().underline(Underline::Single).build(),
                TextTag::builder().strikethrough(true).build(),
                TextTag::builder().family("Source Code Pro").build(),
            ];
            for tag in &tags {
                table.add(tag);
            }
            SharedTags { table, tags }
        });
    }

    #[derive(Downgrade)]
    pub struct FormatAction {
        tag: TextTag,
        button: ToggleButton,
        mark_toggles: Rc<RefCell<Vec<TextMark>>>,
    }

    impl FormatAction {
        fn new(tag: TextTag, icon_name: &str) -> Self {
            Self {
                tag,
                button: ToggleButton::builder()
                    .icon_name(icon_name)
                    .focus_on_click(false)
                    .build(),
                mark_toggles: Default::default(),
            }
        }

        fn add_mark_toggle_at(&self, buffer: &TextBuffer, iter: &TextIter) {
            let mark = TextMark::new(None, true);
            buffer.add_mark(&mark, iter);
            self.mark_toggles.borrow_mut().push(mark);
        }

        fn remove_mark_toggle_at(&self, buffer: &TextBuffer, iter: &TextIter) -> bool {
            let position = self
                .mark_toggles
                .borrow()
                .iter()
                .position(|mark| &buffer.iter_at_mark(mark) == iter);
            if let Some(position) = position {
                buffer.delete_mark(&self.mark_toggles.borrow_mut().remove(position));
                true
            } else {
                false
            }
        }

        fn has_mark_toggle_at(&self, buffer: &TextBuffer, iter: &TextIter) -> bool {
            self.mark_toggles
                .borrow()
                .iter()
                .any(|mark| &buffer.iter_at_mark(mark) == iter)
        }
    }

    fn new_buffer_and_format_actions() -> (TextBuffer, [FormatAction; TAGS_COUNT]) {
        SHARED_TAGS.with(|shared_tags| {
            let buffer = TextBuffer::new(Some(&shared_tags.table));
            (
                buffer,
                [
                    (&shared_tags.tags[0], "format-text-bold-symbolic"),
                    (&shared_tags.tags[1], "format-text-italic-symbolic"),
                    (&shared_tags.tags[2], "format-text-underline-symbolic"),
                    (&shared_tags.tags[3], "format-text-strikethrough-symbolic"),
                    (&shared_tags.tags[4], "code-symbolic"),
                ]
                .map(|(tag, icon_name)| FormatAction::new(tag.clone(), icon_name)),
            )
        })
    }

    #[derive(CompositeTemplate, Properties)]
    #[properties(wrapper_type = super::RichTextEditor)]
    #[template(resource = "/com/gitlab/antnm/Engram/ui/rich_text_editor.ui")]
    pub struct RichTextEditor {
        #[template_child]
        text_view: TemplateChild<TextView>,
        #[template_child]
        action_bar: TemplateChild<ActionBar>,
        buffer: TextBuffer,
        format_actions: [FormatAction; 5],
        #[property(get = |s: &Self| {
            let buffer = s.text_view.buffer();
            buffer
                .text(&buffer.start_iter(), &buffer.end_iter(), false)
                .to_string()
        })]
        text: PhantomData<String>,
    }

    impl Default for RichTextEditor {
        fn default() -> Self {
            let (buffer, format_actions) = new_buffer_and_format_actions();
            Self {
                text_view: Default::default(),
                action_bar: Default::default(),
                buffer,
                format_actions,
                text: PhantomData,
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RichTextEditor {
        const NAME: &'static str = "RichTextEditor";
        type Type = super::RichTextEditor;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
            callbacks::Utils::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    fn selection_has_tag(start: &TextIter, end: &TextIter, tag: &TextTag) -> bool {
        if start.has_tag(tag) {
            let mut toggle = *start;
            toggle.forward_to_tag_toggle(Some(tag));
            if &toggle >= end {
                return true;
            }
        }
        false
    }

    #[template_callbacks]
    impl RichTextEditor {
        fn insert_text(&self, end: &TextIter, text: &str) {
            let buffer = self.text_view.buffer();
            let mut start = *end;
            start.backward_chars(text.len() as i32);
            for format_action in &self.format_actions {
                if format_action.button.is_active() {
                    buffer.apply_tag(&format_action.tag, &start, end);
                } else {
                    buffer.remove_tag(&format_action.tag, &start, end);
                }
                format_action.remove_mark_toggle_at(&buffer, &start);
            }
        }

        fn mark_set(&self, buffer: &TextBuffer, iter: &TextIter, mark: &TextMark) {
            if mark.name().map_or(false, |name| name == "insert") {
                return;
            }
            if let Some((start, end)) = buffer.selection_bounds() {
                for format_action in &self.format_actions {
                    if selection_has_tag(&start, &end, &format_action.tag) {
                        format_action.button.set_active(true);
                    } else {
                        format_action.button.set_active(false);
                    }
                }
            } else {
                let mut backward = *iter;
                backward.backward_char();
                let is_start = iter.is_start();
                for format_action in &self.format_actions {
                    let has_mark_toggle = format_action.has_mark_toggle_at(buffer, iter);
                    let has_tag = !is_start && backward.has_tag(&format_action.tag);
                    if has_tag && !has_mark_toggle || !has_tag && has_mark_toggle {
                        format_action.button.set_active(true);
                    } else {
                        format_action.button.set_active(false);
                    }
                }
            }
        }

        fn apply_tag(&self, format_action: &FormatAction) {
            let buffer = self.text_view.buffer();
            if let Some((start, end)) = buffer.selection_bounds() {
                if selection_has_tag(&start, &end, &format_action.tag) {
                    buffer.remove_tag(&format_action.tag, &start, &end);
                } else {
                    buffer.apply_tag(&format_action.tag, &start, &end);
                }
            } else {
                let insert = buffer.iter_at_mark(&buffer.get_insert());
                if !format_action.remove_mark_toggle_at(&buffer, &insert) {
                    format_action.add_mark_toggle_at(&buffer, &insert);
                }
                format_action
                    .button
                    .set_active(!format_action.button.is_active());
            }
        }

        #[template_callback]
        fn insert_media(&self) {
            WINDOW.with(|window| {
                let window = window.get().unwrap();
                let dialog = FileChooserDialog::new(
                    Some("Choose media"),
                    Some(window),
                    FileChooserAction::Open,
                    &[("Add", ResponseType::Accept)],
                );
                dialog.present();
                let filter = FileFilter::new();
                filter.add_mime_type("image/*");
                filter.add_mime_type("video/*");
                filter.add_mime_type("audio/*");
                dialog.set_filter(&filter);
                let text_view = self.text_view.get();
                dialog.connect_response(
                    glib::clone!(@weak text_view, @weak window => move |dialog, response| {
                        if response == ResponseType::Accept {
                            let file = dialog.file().unwrap();
                            let content_type = file
                                .query_info(
                                    gio::FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                    gio::FileQueryInfoFlags::NONE,
                                    None::<&gio::Cancellable>,
                                )
                                .unwrap()
                                .content_type()
                                .unwrap();
                            let mime_type = gio::content_type_get_mime_type(&content_type).unwrap();
                            let widget;
                            if mime_type.starts_with("image/") {
                                widget = new_image(&file);
                            } else if mime_type.starts_with("video/") {
                                widget = new_video(&file);
                            } else if mime_type.starts_with("audio/") {
                                widget = new_audio(&file);
                            } else {
                                unreachable!();
                            }
                            if let Some(widget) = widget {
                                let buffer = text_view.buffer();
                                let anchor = buffer.create_child_anchor(
                                    &mut buffer.iter_at_mark(&buffer.get_insert()),
                                );
                                text_view.add_child_at_anchor(&widget, &anchor);
                            } else {
                                let message_dialog = MessageDialog::builder()
                                    .heading("Could not open file")
                                    .body(format!(
                                        "The file “{}” could not be opened",
                                        file.basename().unwrap().to_str().unwrap()
                                    ))
                                    .transient_for(&window)
                                    .default_response("close")
                                    .build();
                                message_dialog.add_response("close", "OK");
                                message_dialog.present();
                            }
                        }
                        dialog.destroy();
                    }),
                );
            });
        }
    }

    fn new_image(file: &File) -> Option<Widget> {
        let texture = Texture::from_file(file).ok()?;
        let picture = Picture::for_paintable(&texture);
        let paintable = picture.paintable().unwrap();
        set_picture_request_size(&picture, &paintable);
        let fullscreen_widget = FullscreenWidget::new(picture, None::<Widget>);
        Some(fullscreen_widget.upcast())
    }

    fn new_video(file: &File) -> Option<Widget> {
        let media_file = MediaFile::for_file(file);
        if let Some(error) = media_file.error() {
            log::info!("Could not open video: {}", error);
            return None;
        }
        let video_controls = VideoControls::new(&media_file);
        let picture = Picture::for_paintable(&media_file);
        media_file.play();
        set_picture_request_size(&picture, &media_file);
        media_file.connect_invalidate_size(glib::clone!(@weak picture => move |media_file| {
            set_picture_request_size(&picture, media_file);
        }));
        let fullscreen_widget = FullscreenWidget::new(picture, Some(video_controls.clone()));
        video_controls.connect_local(
            "toggle-fullscreen",
            false,
            glib::clone!(@weak fullscreen_widget => @default-return None, move |_| {
                fullscreen_widget.toggle_fullscreen();
                None
            }),
        );
        Some(fullscreen_widget.upcast())
    }

    fn new_audio(file: &File) -> Option<Widget> {
        let media_file = MediaFile::for_file(file);
        if let Some(error) = media_file.error() {
            log::info!("Could not open audio: {}", error);
            return None;
        }
        let audio_controls = AudioControls::new(&media_file);
        media_file.play();
        Some(audio_controls.upcast())
    }

    fn set_picture_request_size(picture: &Picture, paintable: &impl PaintableExt) {
        let intrinsic_width = paintable.intrinsic_width();
        if intrinsic_width == 0 {
            picture.set_size_request(250, 250);
        } else {
            let width = 250.min(intrinsic_width);
            let height = (width as f64 / paintable.intrinsic_aspect_ratio()) as i32;
            picture.set_size_request(width, height);
        }
    }

    impl super::RichTextEditor {
        pub fn new(app: &Application) -> Self {
            Object::builder().property("application", app).build()
        }

        pub fn clear(&self) {
            self.imp().text_view.buffer().set_text("");
        }
    }

    impl ObjectImpl for RichTextEditor {
        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec);
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            self.text_view.set_buffer(Some(&self.buffer));
            self.buffer.connect_notify_local(
                Some("text"),
                glib::clone!(@weak obj =>
                    move |_, _| obj.notify("text")
                ),
            );
            self.buffer.connect_mark_set(glib::clone!(@weak obj =>
                move |buffer, iter, mark| obj.imp().mark_set(buffer,iter,mark)
            ));
            for format_action in &self.format_actions {
                self.action_bar.pack_start(&format_action.button);
                format_action.button.connect_clicked(
                    glib::clone!(@weak format_action, @weak obj => move |_| {
                        obj.imp().apply_tag(&format_action);
                    }),
                );
            }
            self.text_view.buffer().connect_local(
                "insert-text",
                true,
                glib::clone!(@weak obj => @default-return None, move |args| {
                    obj.imp().insert_text(args[1].get().unwrap(), args[2].get().unwrap());
                    None
                }),
            );
        }
    }

    impl WidgetImpl for RichTextEditor {}

    impl BoxImpl for RichTextEditor {}
}

glib::wrapper! {
    pub struct RichTextEditor(ObjectSubclass<imp::RichTextEditor>)
        @extends gtk::Box, gtk::Widget;
}
