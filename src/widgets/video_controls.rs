mod imp {
    use adw::{prelude::*, subclass::prelude::*};
    use glib::{subclass::Signal, Object, ParamSpec, ParamSpecObject};
    use gtk::{template_callbacks, Adjustment, Box, Button, CompositeTemplate, MediaFile};
    use once_cell::sync::{Lazy, OnceCell};

    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/com/gitlab/antnm/Engram/ui/video_controls.ui")]
    pub struct VideoControls {
        media_file: OnceCell<MediaFile>,
        #[template_child]
        play_button: TemplateChild<Button>,
        #[template_child]
        time_adjustment: TemplateChild<Adjustment>,
        #[template_child]
        volume_adjustment: TemplateChild<Adjustment>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VideoControls {
        const NAME: &'static str = "VideoControls";
        type Type = super::VideoControls;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl super::VideoControls {
        pub fn new(media_file: &MediaFile) -> Self {
            Object::builder().property("media-file", media_file).build()
        }
    }

    #[template_callbacks]
    impl VideoControls {
        #[template_callback]
        fn play_button_clicked(&self) {
            let media_file = self.media_file.get().unwrap();
            let is_playing = !media_file.is_playing();
            media_file.set_playing(is_playing);
            if is_playing {
                self.play_button
                    .set_icon_name("media-playback-pause-symbolic");
            } else {
                self.play_button
                    .set_icon_name("media-playback-start-symbolic");
            }
        }

        #[template_callback]
        fn time_value_changed(&self) {
            let media_file = self.media_file.get().unwrap();
            if self.time_adjustment.value() != (media_file.timestamp() / 1_000_000) as f64 {
                media_file.seek((self.time_adjustment.value() * 1_000_000.0 + 0.5) as i64);
            }
        }

        #[template_callback]
        fn fullscreen_button_clicked(&self) {
            self.obj().emit(SIGNALS[0].signal_id(), &[])
        }
    }

    static SIGNALS: Lazy<Vec<Signal>> =
        Lazy::new(|| vec![Signal::builder("toggle-fullscreen").build()]);

    impl ObjectImpl for VideoControls {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::builder::<MediaFile>("media-file")
                    .construct_only()
                    .build()]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, id: usize, _pspec: &ParamSpec) -> glib::Value {
            match id {
                1 => self.media_file.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(&self, id: usize, value: &glib::Value, _pspec: &ParamSpec) {
            match id {
                1 => {
                    let media_file: MediaFile = value.get().unwrap();
                    media_file
                        .bind_property("timestamp", &*self.time_adjustment, "value")
                        .transform_to(|_, microseconds: i64| {
                            Some((microseconds / 1_000_000) as f64)
                        })
                        .sync_create()
                        .build();
                    media_file
                        .bind_property("duration", &*self.time_adjustment, "upper")
                        .transform_to(|_, microseconds: i64| {
                            Some((microseconds / 1_000_000) as f64)
                        })
                        .sync_create()
                        .build();
                    self.volume_adjustment
                        .bind_property("value", &media_file, "volume")
                        .sync_create()
                        .build();
                    self.media_file.set(media_file).unwrap();
                }
                _ => unimplemented!(),
            }
        }

        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for VideoControls {}

    impl BoxImpl for VideoControls {}
}

glib::wrapper! {
    pub struct VideoControls(ObjectSubclass<imp::VideoControls>)
        @extends gtk::Box, gtk::Widget;
}
