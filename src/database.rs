use ebirsu::{Gamma, Model, Outcome, Quiz};
use rusqlite::{
    functions::{Context, FunctionFlags},
    types::FromSql,
    Connection, ToSql,
};
use std::fs;

const MIGRATIONS: [&str; 1] = [include_str!("migrations/v000_create_decks_and_cards.sql")];

thread_local!(pub static CONNECTION: Connection = new_connection());

fn new_connection() -> Connection {
    let dir = glib::user_data_dir().join("engram");
    fs::create_dir_all(dir.clone()).unwrap();
    let mut conn = Connection::open(dir.join("engram.db")).unwrap();
    conn.execute("PRAGMA foreign_keys = ON", ()).unwrap();
    conn.create_scalar_function(
        "update_recall",
        1,
        FunctionFlags::SQLITE_UTF8,
        update_recall,
    )
    .unwrap();
    let user_version = conn
        .query_row("PRAGMA user_version", (), |row| row.get(0))
        .unwrap();
    if user_version != MIGRATIONS.len() {
        let transaction = conn.transaction().unwrap();
        for migration in &MIGRATIONS[user_version..] {
            transaction.execute_batch(migration).unwrap();
        }
        // Pragmas don't support paramaters
        transaction
            .execute(&format!("PRAGMA user_version = {}", MIGRATIONS.len()), ())
            .unwrap();
        transaction.commit().unwrap();
    }
    conn
}

fn update_recall(ctx: &Context) -> rusqlite::Result<i32> {
    CONNECTION.with(|conn| {
        let card_id = ctx.get(0).unwrap();
        let (mut model, latest_quiz) = fetch_model(conn, card_id);
        model.update_recall(latest_quiz, Default::default(), Default::default());
        model.update_recall_history(Default::default());
        conn.execute(
            "UPDATE cards 
             SET
               init_hl_shape = ?,
               init_hl_rate = ?,
               boost_shape = ?,
               boost_rate = ?,
               current_half_life = ?,
               strength_ln = ?
             WHERE id = ?",
            (
                model.init_hl().shape(),
                model.init_hl().rate(),
                model.boost().shape(),
                model.boost().rate(),
                model.current_half_life(),
                model.strength_ln(),
                card_id,
            ),
        )
        .unwrap();
    });
    Ok(0)
}

fn fetch_quizes(conn: &Connection, card_id: i32, card_created_at: i32) -> Vec<Quiz> {
    let mut stmt = conn
        .prepare(
            "SELECT created_at, binomial_successes, binomial_total, noisy_value
             FROM quizes
             WHERE card = ?
             ORDER BY created_at",
        )
        .unwrap();
    let mut rows = stmt.query((card_id,)).unwrap();
    let mut quizes = Vec::new();
    let mut previous_created_at = card_created_at;
    while let Some(row) = rows.next().unwrap() {
        let created_at = row.get(0).unwrap();
        let elapsed = (created_at - previous_created_at) as f64 / (60.0 * 60.0);
        previous_created_at = created_at;
        let outcome = match row.get(3).unwrap() {
            Some(noisy_value) => Outcome::Noisy(noisy_value),
            None => Outcome::Binomial {
                successes: row.get(1).unwrap(),
                total: row.get(2).unwrap(),
            },
        };
        quizes.push(Quiz::new(elapsed, outcome).unwrap());
    }
    quizes
}

fn fetch_model(conn: &Connection, card_id: i32) -> (Model, Quiz) {
    conn.query_row(
        "SELECT
           created_at,
           init_hl_prior_shape,
           init_hl_prior_rate,
           boost_prior_shape,
           boost_prior_rate,
           init_hl_shape,
           init_hl_rate,
           boost_prior_shape,
           boost_prior_rate,
           current_half_life,
           strength_ln
         FROM cards
         WHERE id = ?",
        (card_id,),
        |row| {
            let card_created_at = row.get(0).unwrap();
            let init_hl_prior = Gamma::new(row.get(1).unwrap(), row.get(2).unwrap()).unwrap();
            let boost_prior = Gamma::new(row.get(3).unwrap(), row.get(4).unwrap()).unwrap();
            let init_hl = Gamma::new(row.get(5).unwrap(), row.get(6).unwrap()).unwrap();
            let boost = Gamma::new(row.get(7).unwrap(), row.get(8).unwrap()).unwrap();
            let current_half_life = row.get(9).unwrap();
            let strength_ln = row.get(10).unwrap();
            let mut quizes = fetch_quizes(conn, card_id, card_created_at);
            let latest_quiz = quizes.pop().unwrap();
            Ok((
                Model::new(
                    &quizes,
                    init_hl_prior,
                    boost_prior,
                    init_hl,
                    boost,
                    current_half_life,
                    strength_ln,
                )
                .unwrap(),
                latest_quiz,
            ))
        },
    )
    .unwrap()
}

pub fn select_column<T: FromSql>(table: &str, column: &str, id: i32) -> T {
    CONNECTION.with(|conn| {
        conn.query_row(
            &format!("SELECT {column} FROM {table} WHERE id = ?"),
            (id,),
            |r| r.get(0),
        )
        .unwrap()
    })
}

pub fn update_column(table: &str, column: &str, id: i32, value: impl ToSql) {
    CONNECTION.with(|conn| {
        conn.execute(
            &format!("UPDATE {table} SET {column} = ? WHERE id = ?"),
            (value, id),
        )
        .unwrap()
    });
}
