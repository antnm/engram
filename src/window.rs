use adw::subclass::prelude::*;
use once_cell::unsync::OnceCell;
use std::ops::Deref;

thread_local!(pub static WINDOW: OnceCell<Window> = OnceCell::new());

mod imp {
    use crate::{
        application::Application,
        callbacks::{AddCardPage, DeckListPage, DeckPage, Utils},
        config::{APP_ID, PROFILE},
        model::{CardStore, Deck, DeckStore},
        widgets::RichTextEditor,
    };
    use adw::{prelude::*, subclass::prelude::*, ApplicationWindow, Leaflet, ToastOverlay};
    use glib::{signal::Inhibit, Object};
    use gtk::{
        Adjustment, Box, Button, CompositeTemplate, Entry, Label, Popover, SingleSelection,
        SpinButton, Stack, TemplateChild, ToggleButton,
    };

    struct Settings(gio::Settings);

    impl Default for Settings {
        fn default() -> Self {
            Self(gio::Settings::new(APP_ID))
        }
    }

    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/com/gitlab/antnm/Engram/ui/window.ui")]
    pub struct Window {
        #[template_child]
        pub deck_store: TemplateChild<DeckStore>,
        #[template_child]
        pub deck_store_selection: TemplateChild<SingleSelection>,
        #[template_child]
        pub card_store: TemplateChild<CardStore>,
        #[template_child]
        pub add_deck_popover: TemplateChild<Popover>,
        #[template_child]
        pub add_deck_entry: TemplateChild<Entry>,
        #[template_child]
        pub add_deck_button: TemplateChild<Button>,
        #[template_child]
        pub deck_name_edit_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub deck_name_label: TemplateChild<Label>,
        #[template_child]
        pub deck_name_entry: TemplateChild<Entry>,
        #[template_child]
        pub leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub add_card_page: TemplateChild<Box>,
        #[template_child]
        pub toast_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub recall_probability_target_spin_button: TemplateChild<SpinButton>,
        #[template_child]
        pub recall_probability_target_adjustment: TemplateChild<Adjustment>,
        #[template_child]
        pub add_card_leaflet: TemplateChild<Leaflet>,
        #[template_child]
        pub add_card_front_side_toggle_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub add_card_front_text: TemplateChild<RichTextEditor>,
        #[template_child]
        pub add_card_back_text: TemplateChild<RichTextEditor>,
        settings: Settings,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Deck::ensure_type();
            DeckStore::ensure_type();

            klass.bind_template();
            Utils::bind_template_callbacks(klass);
            DeckListPage::bind_template_callbacks(klass);
            DeckPage::bind_template_callbacks(klass);
            AddCardPage::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();

            let window = self.obj();

            super::WINDOW.with(|static_window| static_window.set(window.clone()).unwrap());

            if PROFILE == "Devel" {
                window.add_css_class("devel");
            }

            let width = self.settings.0.int("window-width");
            let height = self.settings.0.int("window-height");
            let is_maximized = self.settings.0.boolean("is-maximized");

            window.set_default_size(width, height);

            if is_maximized {
                window.maximize();
            }
        }
    }

    impl super::Window {
        pub fn new(app: &Application) -> Self {
            Object::builder().property("application", app).build()
        }

        pub fn stack(&self) -> Stack {
            self.imp().stack.get()
        }
    }

    impl WidgetImpl for Window {}

    impl WindowImpl for Window {
        fn close_request(&self) -> Inhibit {
            let save_window_size = || {
                let window = self.obj();

                let (width, height) = window.default_size();
                self.settings.0.set_int("window-width", width)?;
                self.settings.0.set_int("window-height", height)?;

                self.settings
                    .0
                    .set_boolean("is-maximized", window.is_maximized())?;

                Ok::<(), glib::BoolError>(())
            };

            if let Err(err) = save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            self.deck_store.shutdown_confirm_delete_all();

            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends adw::ApplicationWindow, gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Root;
}

impl Deref for Window {
    type Target = imp::Window;

    fn deref(&self) -> &Self::Target {
        self.imp()
    }
}
