mod application;
mod callbacks;
mod widgets;
mod window;
#[rustfmt::skip]
mod config;
mod database;
mod model;

use adw::prelude::*;
use config::{GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE};
use gettextrs::{gettext, LocaleCategory};
use gio::Resource;
use glib::{GlibLogger, GlibLoggerDomain, GlibLoggerFormat};
use log::LevelFilter;

use crate::application::Application;

fn main() {
    // Initialize logger
    static GLIB_LOGGER: GlibLogger =
        GlibLogger::new(GlibLoggerFormat::Plain, GlibLoggerDomain::CrateTarget);
    log::set_logger(&GLIB_LOGGER).unwrap();
    log::set_max_level(LevelFilter::Debug);

    // Prepare i18n
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    gettextrs::textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    glib::set_application_name(&gettext("Engram"));

    let res = Resource::load(RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&res);

    adw::init().unwrap();

    Application::default().run();
}
