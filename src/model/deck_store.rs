mod imp {
    use crate::{database::CONNECTION, model::Deck};
    use gio::{prelude::*, subclass::prelude::*, ListModel};
    use glib::{Object, Type};
    use std::cell::{Cell, RefCell};

    pub struct DeckStore {
        decks: RefCell<Vec<Deck>>,
        deleted_decks: RefCell<Vec<Deck>>,
        already_undone_deletion: Cell<bool>,
    }

    impl Default for DeckStore {
        fn default() -> Self {
            let decks = CONNECTION.with(|conn| {
                let mut stmt = conn
                    .prepare("SELECT id FROM decks_with_cards_count")
                    .unwrap();
                let mut rows = stmt.query(()).unwrap();
                let mut decks = Vec::new();
                while let Some(row) = rows.next().unwrap() {
                    decks.push(Deck::new(row.get(0).unwrap()));
                }
                decks
            });
            Self {
                decks: RefCell::new(decks),
                deleted_decks: Default::default(),
                already_undone_deletion: Cell::new(false),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DeckStore {
        const NAME: &'static str = "DeckStore";
        type Type = super::DeckStore;
        type Interfaces = (ListModel,);
    }

    impl ObjectImpl for DeckStore {}

    impl ListModelImpl for DeckStore {
        fn item_type(&self) -> Type {
            Deck::static_type()
        }

        fn n_items(&self) -> u32 {
            self.decks.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<Object> {
            self.decks
                .borrow()
                .get(position as usize)
                .map(|deck| deck.clone().upcast())
        }
    }

    impl super::DeckStore {
        pub fn add(&self, name: String) {
            let id = CONNECTION.with(|conn| {
                conn.query_row(
                    "INSERT INTO decks (name) VALUES (?) RETURNING id",
                    (name,),
                    |r| r.get(0),
                )
                .unwrap()
            });
            let position = {
                let mut decks = self.imp().decks.borrow_mut();
                let position = decks.len();
                let deck = Deck::new(id);
                decks.push(deck);
                position as u32
            };
            self.items_changed(position, 0, 1);
        }

        pub fn delete(&self, id: i32) {
            let imp = self.imp();
            let position = {
                let mut decks = imp.decks.borrow_mut();
                let position = decks.iter().position(|deck| deck.id() == id).unwrap();
                let deck = decks.remove(position);
                imp.deleted_decks.borrow_mut().push(deck);
                position
            };
            self.items_changed(position as u32, 1, 0);
        }

        pub fn undo_delete(&self) {
            let imp = self.imp();
            let position = {
                let mut decks = imp.decks.borrow_mut();
                let deck = imp.deleted_decks.borrow_mut().remove(0);
                let id = deck.id();
                let position = decks.partition_point(|deck| deck.id() < id);
                decks.insert(position, deck);
                position
            };
            imp.already_undone_deletion.set(true);
            self.items_changed(position as u32, 0, 1);
        }

        pub fn confirm_delete(&self) {
            let imp = self.imp();
            if imp.already_undone_deletion.replace(false) {
                return;
            }
            let id = imp.deleted_decks.borrow_mut().remove(0).id();
            CONNECTION.with(|conn| {
                conn.execute("DELETE FROM decks WHERE id = ?", (id,))
                    .unwrap()
            });
        }

        pub fn shutdown_confirm_delete_all(&self) {
            let deleted_decks = self.imp().deleted_decks.borrow();
            let deleted_ids = deleted_decks.iter().map(|deck| deck.id());
            CONNECTION.with(move |conn| {
                let mut stmt = conn.prepare("DELETE FROM decks WHERE id = ?").unwrap();
                deleted_ids.for_each(|id| {
                    stmt.execute((id,)).unwrap();
                });
            });
        }

        pub fn name_exists(name: &str) -> bool {
            CONNECTION
                .with(move |conn| {
                    conn.query_row(
                        "SELECT EXISTS(SELECT 1 FROM decks WHERE name == ?)",
                        (name,),
                        |row| row.get(0),
                    )
                })
                .unwrap()
        }
    }
}

glib::wrapper! {
    pub struct DeckStore(ObjectSubclass<imp::DeckStore>) @implements gio::ListModel;
}
