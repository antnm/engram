mod imp {
    use glib::{prelude::*, subclass::prelude::*, Object, ParamSpec, Properties, Value};
    use std::{cell::Cell, marker::PhantomData};

    use crate::database::{select_column, update_column};

    #[derive(Default, Properties)]
    #[properties(wrapper_type = super::Card)]
    pub struct Card {
        #[property(get, set, construct)]
        id: Cell<i32>,
        #[property(
            get = |s: &Self| select_column(TABLE, "front", s.id.get()),
            set = |s: &Self, front: &str| update_column(TABLE, "front", s.id.get(), front)
        )]
        front: PhantomData<String>,
        #[property(
            get = |s: &Self| select_column(TABLE, "back", s.id.get()),
            set = |s: &Self, back: &str| update_column(TABLE, "back", s.id.get(), back)
        )]
        back: PhantomData<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Card {
        const NAME: &'static str = "Card";
        type Type = super::Card;
    }

    const TABLE: &str = "cards";

    impl ObjectImpl for Card {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }

        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec);
        }
    }

    impl super::Card {
        pub(in crate::model) fn new(id: i32) -> Self {
            Object::builder().property("id", id).build()
        }
    }
}

glib::wrapper! {
    pub struct Card(ObjectSubclass<imp::Card>);
}
