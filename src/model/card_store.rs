mod imp {
    use crate::{database::CONNECTION, model::Card};
    use gio::{prelude::*, subclass::prelude::*, ListModel};
    use glib::{Object, Type};
    use std::cell::RefCell;

    pub struct CardStore {
        cards: RefCell<Vec<Card>>,
    }

    impl Default for CardStore {
        fn default() -> Self {
            let cards = CONNECTION.with(|conn| {
                let mut stmt = conn.prepare("SELECT id FROM cards").unwrap();
                let mut rows = stmt.query(()).unwrap();
                let mut cards = Vec::new();
                while let Some(row) = rows.next().unwrap() {
                    cards.push(Card::new(row.get(0).unwrap()));
                }
                cards
            });
            Self {
                cards: RefCell::new(cards),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CardStore {
        const NAME: &'static str = "CardStore";
        type Type = super::CardStore;
        type Interfaces = (ListModel,);
    }

    impl ObjectImpl for CardStore {}

    impl ListModelImpl for CardStore {
        fn item_type(&self) -> Type {
            Card::static_type()
        }

        fn n_items(&self) -> u32 {
            self.cards.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<Object> {
            self.cards
                .borrow()
                .get(position as usize)
                .map(|card| card.clone().upcast())
        }
    }

    impl super::CardStore {
        pub fn add(&self, id: i32, position: usize) {
            self.imp()
                .cards
                .borrow_mut()
                .insert(position, Card::new(id));
            self.items_changed(position as u32, 0, 1);
        }
    }
}

glib::wrapper! {
    pub struct CardStore(ObjectSubclass<imp::CardStore>) @implements gio::ListModel;
}
