mod imp {
    use crate::{
        database::{select_column, update_column, CONNECTION},
        model::CardStore,
    };
    use glib::{prelude::*, subclass::prelude::*, Object, ParamSpec, Properties, Value};
    use std::{cell::Cell, marker::PhantomData};

    #[derive(Default, Properties)]
    #[properties(wrapper_type = super::Deck)]
    pub struct Deck {
        #[property(get, set, construct)]
        id: Cell<i32>,
        #[property(
            get = |s: &Self| select_column(TABLE, "name", s.id.get()),
            set = |s: &Self, name: &str| update_column(TABLE, "name", s.id.get(), name)
        )]
        name: PhantomData<String>,
        #[property(
            get = |s: &Self| select_column(TABLE, "cards_to_review_count", s.id.get()),
        )]
        cards_to_review_count: PhantomData<u32>,
        #[property(
            get = |s: &Self| select_column(TABLE, "cards_count", s.id.get()),
        )]
        cards_count: PhantomData<u32>,
        #[property(
            get = |s: &Self| select_column(TABLE, "recall_probability_target_ln", s.id.get()),
            set = |s: &Self, target: f64| {
                update_column(
                    TABLE,
                    "recall_probability_target_ln",
                    s.id.get(),
                    target,
                );
                s.obj().notify("recall-probability-target-ln");
            }
        )]
        recall_probability_target_ln: PhantomData<f64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Deck {
        const NAME: &'static str = "Deck";
        type Type = super::Deck;
    }

    const TABLE: &str = "decks_with_cards_count";

    impl ObjectImpl for Deck {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }

        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec);
        }
    }

    impl super::Deck {
        pub fn new(id: i32) -> Self {
            Object::builder().property("id", &id).build()
        }

        pub fn add_card(&self, card_store: &CardStore, front: &str, back: &str) {
            let id = CONNECTION.with(|conn| {
                conn.query_row(
                    "INSERT INTO cards (deck, front, back) VALUES (?, ?, ?) RETURNING id",
                    (self.imp().id.get(), front, back),
                    |r| r.get(0),
                )
                .unwrap()
            });
            card_store.add(id, 0);
            self.notify("cards-count");
        }
    }
}

glib::wrapper! {
    pub struct Deck(ObjectSubclass<imp::Deck>);
}
