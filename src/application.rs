mod imp {
    use crate::{config::APP_ID, window::Window};
    use adw::{prelude::*, subclass::prelude::*};
    use gdk::Display;
    use glib::Object;
    use gtk::{CssProvider, StyleContext, STYLE_PROVIDER_PRIORITY_APPLICATION};
    use once_cell::sync::OnceCell;

    #[derive(Default)]
    pub struct Application {
        pub window: OnceCell<glib::WeakRef<Window>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    impl ApplicationImpl for Application {
        fn activate(&self) {
            self.parent_activate();

            let app = self.obj();

            if let Some(window) = self.window.get() {
                let window = window.upgrade().unwrap();
                window.present();
                return;
            }

            let window = Window::new(&app);
            self.window
                .set(window.downgrade())
                .expect("Window already set.");
            window.present();
        }

        fn startup(&self) {
            self.parent_startup();

            gtk::Window::set_default_icon_name(APP_ID);

            let provider = CssProvider::new();
            provider.load_from_resource("/com/gitlab/antnm/Engram/ui/style.css");
            if let Some(display) = Display::default() {
                StyleContext::add_provider_for_display(
                    &display,
                    &provider,
                    STYLE_PROVIDER_PRIORITY_APPLICATION,
                );
            }

            self.obj()
                .set_accels_for_action("app.quit", &["<Control>q"]);
        }
    }

    impl AdwApplicationImpl for Application {}
    impl GtkApplicationImpl for Application {}
    impl ObjectImpl for Application {}

    impl Default for super::Application {
        fn default() -> Self {
            gio::Application::default().map_or_else(
                || Object::builder().property("application-id", APP_ID).build(),
                |app| {
                    app.downcast::<super::Application>()
                        .expect("The default application is not of the Application type")
                },
            )
        }
    }
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends adw::Application, gtk::Application, gio::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}
