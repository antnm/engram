CREATE TABLE decks (
  id                           INTEGER PRIMARY KEY,
  name                         TEXT    NOT NULL,
  recall_probability_target_ln REAL    NOT NULL DEFAULT (LN(0.5))
) STRICT;

CREATE TABLE cards (
  id                  INTEGER PRIMARY KEY,
  deck                INTEGER NOT NULL,
  front               TEXT    NOT NULL,
  back                TEXT    NOT NULL,
  created_at          INTEGER NOT NULL DEFAULT (unixepoch('now')),
  reviewed_at         INTEGER NOT NULL DEFAULT (unixepoch('now')),
  init_hl_prior_shape REAL    NOT NULL DEFAULT 1.2, -- 12 hours half-life
  init_hl_prior_rate  REAL    NOT NULL DEFAULT 0.1,
  boost_prior_shape   REAL    NOT NULL DEFAULT 4.5,
  boost_prior_rate    REAL    NOT NULL DEFAULT 3.0,
  init_hl_shape       REAL    NOT NULL DEFAULT 1.2, -- same as init_hl_prior
  init_hl_rate        REAL    NOT NULL DEFAULT 0.1,
  boost_shape         REAL    NOT NULL DEFAULT 4.5, -- same as boost_prior
  boost_rate          REAL    NOT NULL DEFAULT 3.0,
  current_half_life   REAL    NOT NULL DEFAULT 12.0, -- init_hl_prior_shape / init_hl_prior_rate
  strength_ln         REAL    NOT NULL DEFAULT 0.0,
  FOREIGN KEY (deck) REFERENCES decks (id) ON DELETE CASCADE
) STRICT;

CREATE TABLE quizes (
  id                 INTEGER PRIMARY KEY,
  card               INTEGER NOT NULL,
  created_at         INTEGER NOT NULL DEFAULT (unixepoch('now')),
  binomial_successes INTEGER,
  binomial_total     INTEGER,
  noisy_value        REAL,
  FOREIGN KEY (card) REFERENCES cards (id) ON DELETE CASCADE,
  CHECK (
    binomial_successes IS NOT NULL AND binomial_total IS NOT NULL AND noisy_value IS     NULL OR
    binomial_successes IS     NULL AND binomial_total IS     NULL AND noisy_value IS NOT NULL
  )
) STRICT;

CREATE VIEW decks_with_cards_count (
  id,
  name,
  recall_probability_target_ln,
  cards_count,
  cards_to_review_count
) AS
  SELECT
    decks.id,
    decks.name,
    decks.recall_probability_target_ln,
    COUNT(cards.id),
    SUM(
      CASE cards.id IS NULL
        WHEN TRUE THEN 0
        ELSE
          -((unixepoch('now') - cards.reviewed_at) / (60 * 60))
          / cards.current_half_life
          * ln(2)
          + cards.strength_ln
          <= decks.recall_probability_target_ln
      END
    ) AS cards_to_review_count
  FROM decks
  LEFT JOIN cards
  ON decks.id = cards.deck
  GROUP BY decks.id
  ORDER BY cards_to_review_count DESC, name ASC;

CREATE TRIGGER decks_with_cards_count_update_trigger
INSTEAD OF UPDATE ON decks_with_cards_count
BEGIN
  UPDATE decks
  SET
    name = NEW.name,
    recall_probability_target_ln = NEW.recall_probability_target_ln
  WHERE decks.id = NEW.id;
END;

CREATE TRIGGER quizes_insert_trigger
AFTER INSERT ON quizes
BEGIN
  UPDATE cards SET reviewed_at = NEW.created_at WHERE cards.id = NEW.card;
  SELECT update_recall(NEW.card);
END;
